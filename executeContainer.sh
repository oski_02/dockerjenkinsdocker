CONTAINER_NAME=jenkinsdocker
JENKINS_HOST_DIR=jenkinsHome
HOST_PORT=8080
# Create host directory to keep all jenkins configuration
mkdir ${JENKINS_HOST_DIR}
# Run docker container with jenkins and angular-cli
docker run \
-d -p ${HOST_PORT}:8080 --name ${CONTAINER_NAME} -v $PWD/${JENKINS_HOST_DIR}:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock trion/jenkins-docker-client
