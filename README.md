# Dockerizing our tests
## Set up environment
To execute our tests in a docker structure we first install docker. To [know more about](https://docs.docker.com/) docker.
* Unistall old versions:
```
sudo apt-get remove docker docker-engine docker.io
```
	
The contents of /var/lib/docker/ are preserved. the DockerCE package is now called ```docker-ce```

* Set up the repo
```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```
* Install Docker CE
```
sudo apt-get update
sudo apt-get install docker-ce
```
## Rise up he container
We must execute the script *executeContainer.sh*
```
sudo sh executeContainer.sh
```
### Script functionality
You can modify variables as you need inside the script (CONTAINER_NAME, JENKINS_HOST_DIR, HOST_PORT)

- CONTAINER_NAME: the container name you want

- JENKINS_HOST_DIR: in order to not loosing all jenkins configuration when we remove the container, a directory is created in the path from where you execute the script to share the jenkins data and save it in host, this is possible thanks to [volumes](https://docs.docker.com/engine/admin/volumes/volumes/). So any time we rise up any jenkins container with this volume, the jenkins service will get the configuration, jobs, users, etc... from the JENKINS_HOST_DIR
    
    ```
    -v $PWD/${JENKINS_HOST_DIR}:/var/jnekins_home
    ```
    
- HOST_PORT: the host port to mapp to the jenkins port (by default is 8080)

To execute tests, the jenkins container has a pipeline to rise up a container with node and angular-cli packages. But the idea is to make both containers "brothers", both childs from host instead of the new container child of jenkins one. So we create another volume to share the socket:
```
-v /var/run/docker.sock:/var/run/docker.sock trion/jenkins-docker-client
```