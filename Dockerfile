FROM jenkins/jenkins:lts

LABEL description="Jenkins with php, node and karma"

USER root

ENV TERM xterm

# Install Node
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs
RUN npm install -g \
  eslint@3.19.0 \
  jasmine-core@2.6.4 \
  karma@1.7.0 \
  karma-chrome-launcher@2.2.0 \
  karma-coverage@1.1.1 \
  karma-jasmine@1.1.0 \
  karma-jasmine-jquery@0.1.1 \
  karma-junit-reporter@1.2.0 \
  karma-ng-html2js-preprocessor@1.0.0 \
  newman@3.7.4 \
  uglify-js@2.6.2

# Install PHP
RUN apt-get update && apt-get install -y \
  php7.0 \
  php7.0-cli \
  php7.0-common \
  php7.0-curl \
  php7.0-gd \
  php7.0-intl \
  php7.0-json \
  php7.0-mcrypt \
  php7.0-mbstring \
  php7.0-mysql \
  php7.0-mysqlnd \
  php7.0-odbc \
  php7.0-readline \
  php7.0-xdebug \
  libapache2-mod-php7.0 \
  php-codecoverage \
  php-doctrine-instantiator \
  php-file-iterator \
  php-pear \
  php-symfony-yaml \
  php-text-template \
  php-timer \
  php-token-stream \
  phpunit-comparator \
  phpunit-diff \
  phpunit-environment \
  phpunit-exporter \
  phpunit-mock-object \
  phpunit-version \
&& rm -rf /var/lib/apt/lists/*

# Install Composer
RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/local/bin --filename=composer

# Install Chrome
RUN apt-get update \
&& apt-get -y install wget \
&& sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb stable main" > /etc/apt/sources.list.d/google.list' \
&& wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
&& apt-get update \
&& apt-get -y install google-chrome-stable \
&& rm -rf /var/lib/apt/lists/*

# Add Jenkins to docker group. This is bad... but required to run the current version of some jenkins jobs that use docker commands
# TODO: Remove that and change the jobs that require it (like "Argo PHP Integration Test Own Server")
RUN groupadd --gid 999 docker \
&& usermod -aG docker jenkins

USER jenkins